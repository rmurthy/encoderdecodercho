function trim(s)
  return s:match'^%s*(.*%S)' or ''
end

function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
   table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

-- http://lua-users.org/wiki/FileInputOutput
-- see if the file exists
function file_exists(file)
  local f = io.open(file, "rb")
  if f then f:close() end
  return f ~= nil
end

-- get all lines from a file, returns an empty 
-- list/table if the file does not exist
function lines_from(file)
  if not file_exists(file) then return {} end
  lines = {}
  for line in io.lines(file) do 
    lines[#lines + 1] = line
  end
  return lines
end

function loadParallelData(fileName, sourceDictionary, targetDictionary)
  local sourceData = {}
  local targetData = {}
  for line in io.lines(fileName) do
    -- extract every word --
    local sourceWord;
    local targetWord;
  
    -- Remember source and target sequence are separated by tab
    local splitWord = split(line,"\t")
    for index,word in ipairs(splitWord) do
      if index == 1 then
        sourceWord = word
      else
        targetWord = word
      end
    end
  
    local sourceLength = 0
    local tempSource = {}
    for word in string.gmatch(sourceWord,"[^ ]+") do
      sourceLength = sourceLength +1
      table.insert(tempSource,word) 
    end
    
    local tempsourceWord = " "
    for i = #tempSource,1,-1 do
      tempsourceWord = tempsourceWord..tempSource[i].." "
    end
    sourceWord = trim(tempsourceWord)
    
--    For end of sentence marker
    sourceLength = sourceLength +1
  
    local targetLength = 0
    for word in string.gmatch(targetWord,"[^ ]+") do
      targetLength = targetLength +1 
    end
--    For end of sentence marker
    targetLength = targetLength +1
  
    local inputSource= torch.Tensor(sourceLength,1):zero()
    local target = torch.Tensor(targetLength):zero()
  
    --read source and target sequence 
    local currentLength = 0;
    local index = 1;
    for word in string.gmatch(sourceWord,"[^ ]+") do
      if(sourceDictionary[word] == nil) then
        print(word.." Not found in source vocabulary")
      else
        index = sourceDictionary[word]
        inputSource[currentLength + 1][1] = index
      end
      currentLength = currentLength +1; 
    end
    
    index = sourceDictionary["</S>"]
    inputSource[currentLength + 1][1] = index
  
    currentLength = 0;
    index = 1;
    for word in string.gmatch(targetWord,"[^ ]+") do
      if(targetDictionary[word] == nil) then
        print(word.." Not found in target vocabulary")
      else
        index = targetDictionary[word]
        target[currentLength + 1] = index 
      end
      currentLength = currentLength +1; 
    end
    
    index = targetDictionary["</S>"]
    target[currentLength + 1] = index
  
    table.insert(sourceData, inputSource)
    table.insert(targetData, target)
  end
  return sourceData, targetData
end