
require("vocabularyBuilder")
require("utilities")
require "torch"
require "nn"
require "sys"
require 'optim'
require 'xlua'   
local LSTM = require 'LSTM'             -- LSTM timestep and utilities
local LSTM_Decoder = require 'LSTM_Decoder'             -- LSTM timestep and utilities
require 'beamSearch'             -- LSTM timestep and utilities
local model_utils=require 'model_utils'
local w_init=require("weight-init")

-- Use Float instead of Double
torch.setdefaulttensortype('torch.FloatTensor')

cmd = torch.CmdLine()
cmd:text()
cmd:text()
cmd:text('Training a Neural Network for End-to-end Sequence Translation')
cmd:text()
cmd:text('Options')
cmd:option('-train',"","Path to Train File -- Source and Target Sequence on same line separated by Tab")
cmd:option('-tune',"","Path to Development File -- Source and Target Sequence on same line separated by Tab")
cmd:option('-test',"","Path to Test File -- Source and Target Sequence on same line separated by Tab")
cmd:option('-source',"","Path to Full Source File for building Vocabulary")
cmd:option('-target',"","Path to Full Target File for building Vocabulary")
cmd:option('-hiddenSize',200,"Size of Hidden Layer Neurons")
cmd:option('-embeddingSize',50,"Size of Word Embeddings")
cmd:option('-predict',false,"Perform Only Prediction, no training")
cmd:option('-languagePairs',"","Language Pairs")
cmd:option('-preLoadModule',"","intermediate module filename")
cmd:option('-intermediate',false,"Start training using pretrained module")
cmd:option('-useGPU',false,"Use GPU")
cmd:option('-optimizationTechnique',0, "Optimization Technique to be used")
cmd:text()

local options = cmd:parse(arg)

print("using Full Source File "..options.source)
print("using Full Target File "..options.target)
print("using Train File "..options.train)
print("using Tune File "..options.tune)
print("using Test File "..options.test)
print("using Hidden Layer Neurons "..options.hiddenSize)
print("using Embedding Size "..options.embeddingSize)

if options.useGPU then
  print("using GPU ")
  require 'cunn'
  require 'cutorch'
  cutorch.setDevice(1)
else
  print("using CPU ")
end

--Create Source Vocabulary and collect other information--
local sourceDictionary, reverseSourceDictionary, sourceMaxLength, sourceDictionarySize = createVocabulary(options.source)
print("Max Source Sequence Length "..sourceMaxLength.." and vocabulary size "..sourceDictionarySize)

--Create Target Vocabulary and collect other information--
local targetDictionary, reverseTargetDictionary, targetMaxLength, targetDictionarySize, EOS = createVocabulary(options.target)
print("Max Target Sequence Length "..targetMaxLength.." and vocabulary size "..targetDictionarySize)

local trainDataSource, trainDataTarget = loadParallelData(options.train, sourceDictionary, targetDictionary)
local tuneDataSource, tuneDataTarget = loadParallelData(options.tune, sourceDictionary, targetDictionary)
local testDataSource, testDataTarget = loadParallelData(options.test, sourceDictionary, targetDictionary)

print("Training data contains "..#trainDataSource.." Parallel Pairs")
print("Development data contains "..#tuneDataSource.." Parallel Pairs")
print("Test data contains "..#testDataSource.." Parallel Pairs")

local protos = {}
local function w_init_xavier_caffe(fan_in, fan_out)
   return math.sqrt(1/fan_in)
end

-- Send the input through a Lookup Table first to obtain it's embeddings
if options.useGPU then
  protos.embed = nn.Sequential():add(nn.Copy('torch.FloatTensor', 'torch.CudaTensor')):add(nn.LookupTable(sourceDictionarySize, options.embeddingSize)):add(nn.Reshape(options.embeddingSize))
else  
  protos.embed = nn.Sequential():add(nn.LookupTable(sourceDictionarySize, options.embeddingSize)):add(nn.Reshape(options.embeddingSize))
end
-- LSTM Encoder takes the current input and embeddings and encodes the sequence seen till now
protos.lstm = LSTM.lstm(options.embeddingSize, options.hiddenSize)
-- LSTM Decoder takes the summary from LSTM Encoder generates the target sequence
protos.lstm_out = LSTM_Decoder.lstm(targetDictionarySize + options.hiddenSize,options.hiddenSize)
-- Use LogSoftmax to obtain probabilities for training
if options.useGPU then
	protos.softmax = nn.Sequential():add(nn.Linear(options.hiddenSize + options.hiddenSize + targetDictionarySize, targetDictionarySize)):add(nn.LogSoftMax()):add(nn.Copy('torch.CudaTensor', 'torch.FloatTensor'))
else
	protos.softmax = nn.Sequential():add(nn.Linear(options.hiddenSize + options.hiddenSize + targetDictionarySize, targetDictionarySize)):add(nn.LogSoftMax())	
end
protos.exp = nn.Sequential():add(nn.Exp())
protos.exp1 = nn.Sequential():add(nn.Exp())
protos.criterion = nn.ClassNLLCriterion()

if options.useGPU then
	protos.embed:cuda()
	protos.lstm:cuda()
	protos.lstm_out:cuda()
	protos.softmax:cuda()
	protos.exp:cuda()
	protos.exp1:cuda()
end

-- put the above things into one flattened parameters tensor
local params, grad_params = model_utils.combine_all_parameters( protos.embed, protos.lstm,protos.lstm_out, protos.softmax)
params:uniform(-0.08, 0.08)

local method = "xavier_caffe"
protos.embed  = require('weight-init')(protos.embed, method)
protos.softmax = require('weight-init')(protos.softmax, method)
params, grad_params = model_utils.combine_all_parameters( protos.embed, protos.lstm,protos.lstm_out, protos.softmax)

-- Use SGD with Nesterov Momentum
local optimState = {
      learningRate = 0.01, -- Best was 0.1
      weightDecay = 0.0001,
      momentum = 0.001, 
      learningRateDecay = 0.0,
      nesterov = true,
      dampening = 0.0,
        alpha = 0.95
   }
local optimMethod
if options.optimizationTechnique == 0 then
   optimMethod = optim.sgd
   print("Using SGD with Momentum")
elseif options.optimizationTechnique == 1 then
  optimMethod = optim.adagrad
  print("Using Adagrad")
elseif options.optimizationTechnique == 2 then
  optimMethod = optim.rmsprop
  print("Using RMSprop")
end

-- make a bunch of clones, AFTER flattening, as that reallocates memory
local clones = {}

-- LSTM initial state (zero initially)
local initstate_c = torch.zeros( options.hiddenSize)
local initstate_h = initstate_c:clone()

-- LSTM final state's backward message (dloss/dfinalstate) is 0, since it doesn't influence predictions
local dfinalstate_c = initstate_c:clone()
local dfinalstate_h = initstate_c:clone()

-- LSTM initial state (zero initially)
local initstate_c_out = torch.zeros( options.hiddenSize)
local initstate_h_out = initstate_c:clone()

-- LSTM final state's backward message (dloss/dfinalstate) is 0, since it doesn't influence predictions
local dfinalstate_c_out = initstate_c:clone()
local dfinalstate_h_out = initstate_c:clone()

function performTraining(inputSource, target)
------------------- forward pass -------------------
  local embeddings = {}            -- input embeddings
  local lstm_c = {[0]=initstate_c} -- internal cell states of Encoder LSTM
  local lstm_h = {[0]=initstate_h} -- output values of Encoder LSTM
  
  local lstm_c_out = {[0]=initstate_c_out} -- internal cell states of Decoder LSTM1
  local lstm_h_out = {[0]=initstate_h_out} -- output values of Decoder LSTM1
  
  local predictions = {}           -- softmax outputs
  local inputLSTM_Out = {}
  local decoderInput = {}
  local predictionsExp = {}
  local predictionsExp1 = {}
  local dPredictionsExp = {}
  local dPredictionsExp1 = {}
  
  local loss = 0

  -- Send the input sequence through an LSTM Encoder
  for t=1,inputSource:size(1) do
     embeddings[t] = clones.embed[t]:forward(inputSource[t])[1]
     if options.useGPU then
      lstm_c[t], lstm_h[t] = unpack(clones.lstm[t]:forward({embeddings[t], lstm_c[t-1]:cuda(), lstm_h[t-1]:cuda()}))
     else
      lstm_c[t], lstm_h[t] = unpack(clones.lstm[t]:forward({embeddings[t], lstm_c[t-1], lstm_h[t-1]}))
     end
  end
  
  -- Last Hidden State of Encoder is the summary of the Source Sequence
  local summary = lstm_h[inputSource:size(1)]:clone()
  summary = summary:reshape(summary:size(1))
  
  -- Use a Decoder for Prediction target sequence one by one
  for t=1,target:size(1) do
    -- Previous prediction is zero for time t = 1
    -- input is y_{t-1} summary
    if t == 1 then
      if options.useGPU then
        inputLSTM_Out[t] = torch.cat(torch.zeros(targetDictionarySize), summary:float())
        lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t]:cuda(), lstm_c_out[t-1]:cuda(), lstm_h_out[t-1]:cuda()}))
      else
        inputLSTM_Out[t] = torch.cat(torch.zeros(targetDictionarySize), summary:float())
        lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t], lstm_c_out[t-1], lstm_h_out[t-1]}))
      end
    else 
      -- input is y_{t-1} summary
      if options.useGPU then
        inputLSTM_Out[t] = torch.cat(predictionsExp[t-1]:float(), summary:float())
        lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t]:cuda(), lstm_c_out[t-1]:cuda(), lstm_h_out[t-1]:cuda()}))
      else
        inputLSTM_Out[t] = torch.cat(predictionsExp[t-1]:float(), summary:float())
        lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t], lstm_c_out[t-1], lstm_h_out[t-1]}))
      end
    end
    
    -- input is summary h_{t} y_{t-1}
    local temp = torch.cat(summary:float(), lstm_h_out[t]:float()):clone()
    if t ~= 1 then
      decoderInput[t] = torch.cat(temp, predictionsExp[t-1]:float())
    else
      local temp1 = torch.zeros(targetDictionarySize)
      decoderInput[t] = torch.cat(temp, temp1:float())
    end
    
    predictions[t] = clones.softmax[t]:forward(decoderInput[t]:cuda())
    -- output is logSoftmax, so use exp to convert it back to probabilities
    predictionsExp[t] = clones.exp[t]:forward(predictions[t]):clone()
    predictionsExp1[t] = clones.exp1[t]:forward(predictions[t]):clone()
    
    loss = loss + clones.criterion[t]:forward(predictions[t]:float(), target[t])
  end

  ------------------ backward pass -------------------
  -- complete reverse order of the above
  local dembeddings = {}                              -- d loss / d input embeddings
  local dlstm_c = {[inputSource:size(1)]=dfinalstate_c}    -- internal cell states of LSTM
  local dlstm_h = {}                                  -- output values of LSTM
  
  local dSummary = torch.zeros(options.hiddenSize)                              -- d loss / d summary
  if options.useGPU then
    dSummary = dSummary:cuda()
  end
  local dTempSummary = {}                              -- d loss / d summary
  local dlstm_c_out = {}    -- internal cell states of LSTM
  if options.useGPU then
    dlstm_c_out[target:size(1)]=dfinalstate_c_out:cuda()
  end
  local dlstm_h_out = {}                                  -- output values of LSTM
  if options.useGPU then
    dlstm_h_out[target:size(1)]= dfinalstate_h_out:cuda()
  end
  local dDecoder = {}
  local dOutput = {[target:size(1)]=torch.zeros(targetDictionarySize)}
  
  for t=target:size(1),1,-1 do
    dOutput[t]:add(clones.criterion[t]:backward(predictions[t]:float(), target[t]):clone())
    
    -- Jordan Neural Network
    if options.useGPU then 
      dDecoder[t] = clones.softmax[t]:backward(decoderInput[t]:cuda(), dOutput[t]:cuda()):clone()
    else
      dDecoder[t] = clones.softmax[t]:backward(decoderInput[t], dOutput[t]):clone()
    end
      
    -- input is summary h_{t} y_{t-1}
    dSummary:add(dDecoder[t]:narrow(1,1,options.hiddenSize))
    dlstm_h_out[t]:add(dDecoder[t]:narrow(1, options.hiddenSize+1, options.hiddenSize))
    
    if t ~= 1 then
      dPredictionsExp1[t-1] = clones.exp1[t]:backward(predictions[t-1]:float(), dDecoder[t]:narrow(1, options.hiddenSize *2+1, targetDictionarySize))
      if dOutput[t-1] ~= nil then 
        dOutput[t-1]:add(dPredictionsExp1[t-1]:float())
      else
        dOutput[t-1] = dPredictionsExp1[t-1]:float()
      end
    end

    -- backprop through LSTM timestep
    if options.useGPU then 
      dTempSummary[t], dlstm_c_out[t-1], dlstm_h_out[t-1] = unpack(clones.lstm_out[t]:backward(
          {inputLSTM_Out[t]:cuda(), lstm_c_out[t-1]:cuda(), lstm_h_out[t-1]:cuda()},
          {dlstm_c_out[t]:cuda(), dlstm_h_out[t]:cuda()}
      ))
    else
      dTempSummary[t], dlstm_c_out[t-1], dlstm_h_out[t-1] = unpack(clones.lstm_out[t]:backward(
        {inputLSTM_Out[t], lstm_c_out[t-1], lstm_h_out[t-1]},
        {dlstm_c_out[t], dlstm_h_out[t]}
      ))
    end
      
    -- input is y_{t-1} summary
    dSummary:add(dTempSummary[t]:narrow(1,targetDictionarySize+1,options.hiddenSize))
        
    local temp = dTempSummary[t]:narrow(1,1,targetDictionarySize)
    
    if t ~= 1 then
      dPredictionsExp[t-1] = clones.exp[t]:backward(predictions[t-1]:float(), temp)
      
      if dOutput[t-1] ~= nil then 
        dOutput[t-1]:add(dPredictionsExp[t-1]:float())
      else
        dOutput[t-1] = dPredictionsExp[t-1]:float()
      end
    end 
  end

  for t=inputSource:size(1),1,-1 do
      -- the error from higher layer is sent only to the last hidden layer
      if t == inputSource:size(1) then
          assert(dlstm_h[t] == nil)
          dlstm_h[t] = dSummary:clone()
      end

      -- backprop through LSTM timestep
      if options.useGPU then
        dembeddings[t], dlstm_c[t-1], dlstm_h[t-1] = unpack(clones.lstm[t]:backward(
            {embeddings[t]:cuda(), lstm_c[t-1]:cuda(), lstm_h[t-1]:cuda()},
            {dlstm_c[t]:cuda(), dlstm_h[t]:cuda()}
        ))
      else
        dembeddings[t], dlstm_c[t-1], dlstm_h[t-1] = unpack(clones.lstm[t]:backward(
          {embeddings[t], lstm_c[t-1], lstm_h[t-1]},
          {dlstm_c[t], dlstm_h[t]}
        ))
      end
      
      -- backprop through embeddings
      clones.embed[t]:backward(inputSource[t]:float(), dembeddings[t])
      -- End of for loop
  end
  return loss
  -- End of training mini-batch

end

function test(epoch)
  local f = assert(io.open("output/"..options.languagePairs.."/test_out_"..epoch, "w"))

  local cost = 0.0
  local lineCount = 0
  
  for pairs = 1,#testDataSource do
    local sourceWord = " " 
    
    for i =1,testDataSource[pairs]:size(1) do
      sourceWord = sourceWord.." "..reverseSourceDictionary[testDataSource[pairs][i][1]]
    end
    
    local targetWord = " " 
    
    for i =1,testDataTarget[pairs]:size(1) -1 do
      targetWord = targetWord.." "..reverseTargetDictionary[testDataTarget[pairs][i]]
    end
    
    f:write(sourceWord.."\t"..targetWord.."\t")
    print(sourceWord.."\t"..targetWord.."\t")
      
    local inputSource = testDataSource[pairs]
    local target = testDataTarget[pairs] 
      
    local embeddings = {}            -- input embeddings
    local lstm_c = {[0]=initstate_c} -- internal cell states of Encoder LSTM
    local lstm_h = {[0]=initstate_h} -- output values of Encoder LSTM
    
    local lstm_c_out = {[0]=initstate_c_out} -- internal cell states of Decoder LSTM1
    local lstm_h_out = {[0]=initstate_h_out} -- output values of Decoder LSTM1
    
    local predictions = {}           -- softmax outputs
    local loss = 0

    -- Send the input sequence through an LSTM Encoder
    for t=1,inputSource:size(1) do
       embeddings[t] = clones.embed[t]:forward(inputSource[t])[1]
       if options.useGPU then 
        lstm_c[t], lstm_h[t] = unpack(clones.lstm[t]:forward({embeddings[t], lstm_c[t-1]:cuda(), lstm_h[t-1]:cuda()}))
       else
        lstm_c[t], lstm_h[t] = unpack(clones.lstm[t]:forward({embeddings[t], lstm_c[t-1], lstm_h[t-1]}))
       end
    end
    
    -- Last Hidden State of Encoder is the summary of the Source Sequence 
    local summary = lstm_h[inputSource:size(1)]:clone()
    summary = summary:reshape(summary:size(1))
    
    local predictedSequence,cost = doBeamSearch(summary, clones, options, targetDictionarySize, EOS, reverseTargetDictionary, options.useGPU)
    
    local predictedOutput = {}
    for i=1, #predictedSequence do
      local outputWord = reverseTargetDictionary[predictedSequence[i]]
      f:write(outputWord.." ")
      table.insert(predictedOutput, outputWord)
    end
    
    print(predictedOutput)
    print(cost)
    
    f:write("\n")
  end
  f:close()
end


function train(epoch, cos)
  local previousCost = cos
  local miniBatch = 25
  local Epoch = epoch
    
--  For every sweep of training data
  while Epoch < 200 do
    print("==> doing epoch "..Epoch.." on training data with eta :"..optimState.learningRate)
    print("Previous Cost "..previousCost)
    nClock = os.clock()
    
    local batch = 0;
    local batchCounter = 0;
    
    local batchInput= {};     
    local batchTarget= {}; 
    
    for pairs =1,#trainDataSource do
      xlua.progress(pairs, #trainDataSource)
      
      table.insert(batchInput, trainDataSource[pairs])
      table.insert(batchTarget, trainDataTarget[pairs])
      
      batch = batch + 1;
      
      if batch == miniBatch then
        local loss = 0
        local feval = function(params_)
          if params_ ~= params then
              params:copy(params_)
          end
          grad_params:zero()
    
          for j =1,miniBatch do
            local inputSource = batchInput[j]
            local target = batchTarget[j]
          
            loss = loss + performTraining(inputSource,target)  
          end
          
          grad_params:div(miniBatch)
          loss = loss /miniBatch
          
          -- clip gradient element-wise
          grad_params:clamp(-2, 2)
      
          return loss, grad_params
        end
        optimMethod(feval, params, optimState)
        batch = 0;
        batchCounter = batchCounter +1
      
        batchInput = {}
        batchTarget = {}
      end
    end
    
    if batch ~= 0 then
      local loss = 0
      local feval = function(params_)
        if params_ ~= params then
            params:copy(params_)
        end
        grad_params:zero()
  
        for j =1,batch do
          local inputSource = batchInput[j]
          local target = batchTarget[j]
          loss = loss + performTraining(inputSource,target)
        end
        
        grad_params:div(batch)
        loss = loss /batch
        
        -- clip gradient element-wise
        grad_params:clamp(-2, 2)
    
        return loss, grad_params
      end
      
      optimMethod(feval, params, optimState)
      batch = 0;
      batchCounter = batchCounter +1
    
      batchInput = {}
      batchTarget = {}
    end
    
    local cost = 0.0
    cost = validation(tuneDataSource, tuneDataTarget)
    
    print("Elapsed time: " .. os.clock()-nClock)
    
    if Epoch == 0 then
      previousCost = cost
      
      local filename = "output/"..options.languagePairs.."/optimState_"..Epoch
      os.execute('mkdir -p ' .. sys.dirname(filename))
      torch.save(filename, optimState)
      
      filename = "output/"..options.languagePairs.."/protos_"..Epoch
      torch.save(filename, protos)
      
      Epoch = Epoch + 1
    else
      if cost >= previousCost then
        
        local filename = "output/"..options.languagePairs.."/protos_"..(Epoch-1)
        protos = torch.load(filename)
        
        filename = "output/"..options.languagePairs.."/optimState_"..(Epoch-1)
        optimState = torch.load(filename)
        
        if options.useGPU then 
          protos.embed:cuda()
          protos.lstm:cuda()
          protos.lstm_out:cuda()
          protos.softmax:cuda()
          protos.exp:cuda()
        end
        
        params, grad_params = model_utils.combine_all_parameters( protos.embed, protos.lstm,protos.lstm_out, protos.softmax)
        
        clones = {}
        for name,proto in pairs(protos) do
            print('cloning '..name)
            clones[name] = model_utils.clone_many_times(proto, math.max(targetMaxLength,sourceMaxLength)+1, not proto.parameters)
        end
        
        local learningRate = optimState.learningRate * 0.7
        optimState.learningRate = learningRate
        print("Learning Rate Reduced "..learningRate)
      else
        previousCost = cost
        
        local filename = "output/"..options.languagePairs.."/optimState_"..Epoch
        os.execute('mkdir -p ' .. sys.dirname(filename))
        torch.save(filename, optimState)
        
        filename = "output/"..options.languagePairs.."/protos_"..Epoch
        torch.save(filename, protos)
        
        if Epoch%10 == 0 then
          test( Epoch)
        end
    
        Epoch = Epoch + 1
      end
    end
    
  end
end

function validation(DataSource, DataTarget)
  local cost = 0.0
  local lineCount = 0;
  
  for pairs =1,#DataSource do
    -- extract every word --
    
    local inputSource = DataSource[pairs]
    local target = DataTarget[pairs] 
    
    local embeddings = {}            -- input embeddings
    local lstm_c = {[0]=initstate_c} -- internal cell states of Encoder LSTM
    local lstm_h = {[0]=initstate_h} -- output values of Encoder LSTM
    
    local lstm_c_out = {[0]=initstate_c_out} -- internal cell states of Decoder LSTM1
    local lstm_h_out = {[0]=initstate_h_out} -- output values of Decoder LSTM1
    
    local predictions = {}           -- softmax outputs
    local inputLSTM_Out = {}
    local decoderInput = {}
    local predictionsExp = {}
    
      -- Send the input sequence through an LSTM Encoder
    for t=1,inputSource:size(1) do
     embeddings[t] = clones.embed[t]:forward(inputSource[t])[1]
     if options.useGPU then 
      lstm_c[t], lstm_h[t] = unpack(clones.lstm[t]:forward({embeddings[t], lstm_c[t-1]:cuda(), lstm_h[t-1]:cuda()}))
     else
      lstm_c[t], lstm_h[t] = unpack(clones.lstm[t]:forward({embeddings[t], lstm_c[t-1], lstm_h[t-1]}))
     end
    end
    
    -- Last Hidden State of Encoder is the summary of the Source Sequence 
    local summary = lstm_h[inputSource:size(1)]:clone()
    summary = summary:reshape(summary:size(1))
      
    -- Use a Decoder for Prediction target sequence one by one
    for t=1,target:size(1) do
      -- Previous prediction is zero for time t = 1
      -- input is y_{t-1} summary
      if t == 1 then
        if options.useGPU then
          inputLSTM_Out[t] = torch.cat(torch.zeros(targetDictionarySize), summary:float()):cuda()
          lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t], lstm_c_out[t-1]:cuda(), lstm_h_out[t-1]:cuda()}))
        else
          inputLSTM_Out[t] = torch.cat(torch.zeros(targetDictionarySize), summary:float())
          lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t], lstm_c_out[t-1], lstm_h_out[t-1]}))
        end
      else 
        -- input is y_{t-1} summary
        if options.useGPU then
          inputLSTM_Out[t] = torch.cat(predictionsExp[t-1]:float(), summary:float()):cuda()
          lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t], lstm_c_out[t-1]:cuda(), lstm_h_out[t-1]:cuda()}))
        else
          inputLSTM_Out[t] = torch.cat(predictionsExp[t-1]:float(), summary:float())
          lstm_c_out[t], lstm_h_out[t] = unpack(clones.lstm_out[t]:forward({inputLSTM_Out[t], lstm_c_out[t-1], lstm_h_out[t-1]}))
        end
      end
      
      -- input is summary h_{t} y_{t-1}
      local temp = torch.cat(summary:float(), lstm_h_out[t]:float()):clone()
      if t ~= 1 then
        if options.useGPU then
          decoderInput[t] = torch.cat(temp, predictionsExp[t-1]:float()):cuda()
        else
          decoderInput[t] = torch.cat(temp, predictionsExp[t-1]:float())
        end
      else
        local temp1 = torch.zeros(targetDictionarySize)
        if options.useGPU then
          decoderInput[t] = torch.cat(temp, temp1:float()):cuda()
        else
          decoderInput[t] = torch.cat(temp, temp1:float())
        end
      end
      
      predictions[t] = clones.softmax[t]:forward(decoderInput[t])
      -- output is logSoftmax, so use exp to convert it back to probabilities
      predictionsExp[t] = clones.exp[t]:forward(predictions[t]):clone()
      
      cost = cost + clones.criterion[t]:forward(predictions[t]:float(), target[t])
    end
  end
  print("Cost on Development set "..(cost/#DataSource))
  return (cost/#DataSource)
end

if options.predict then 
  print("On Prediction Mode")

  local filename = "output/"..options.languagePairs.."/optimState_"..options.preLoadModule
  optimState = torch.load(filename)
   
  filename = "output/"..options.languagePairs.."/protos_"..options.preLoadModule
  protos = torch.load(filename)
  
  if options.useGPU then
    protos.embed:cuda()
    protos.lstm:cuda()
    protos.lstm_out:cuda()
    protos.softmax:cuda()
    protos.exp:cuda()
    protos.exp1:cuda()
  end
 
 -- put the above things into one flattened parameters tensor
  params, grad_params = model_utils.combine_all_parameters( protos.embed, protos.lstm,protos.lstm_out, protos.softmax)
  
  clones = {}
  for name,proto in pairs(protos) do
      print('cloning '..name)
      clones[name] = model_utils.clone_many_times(proto, math.max(targetMaxLength,sourceMaxLength)+1, not proto.parameters)
  end
  
  validation(tuneDataSource, tuneDataTarget)
  test(options.preLoadModule)
else
  local epoch = 0
  local previousCost
  if options.intermediate then
    epoch = options.preLoadModule
    local filename = "output/"..options.languagePairs.."/optimState_"..options.preLoadModule
    optimState = torch.load(filename)
    optimState.learningRate = 0.00343 * 0.7
    
    filename = "output/"..options.languagePairs.."/protos_"..options.preLoadModule
    protos = torch.load(filename)
  
    if options.useGPU then
      protos.embed:cuda()
      protos.lstm:cuda()
      protos.lstm_out:cuda()
      protos.softmax:cuda()
      protos.exp:cuda()
      protos.exp1:cuda()
    end
   
   -- put the above things into one flattened parameters tensor
		params, grad_params = model_utils.combine_all_parameters( protos.embed, protos.lstm,protos.lstm_out, protos.softmax)
	end
  
  clones = {}
  for name,proto in pairs(protos) do
      print('cloning '..name)
      clones[name] = model_utils.clone_many_times(proto, math.max(targetMaxLength,sourceMaxLength)+1, not proto.parameters)
  end
  
  previousCost = validation(tuneDataSource, tuneDataTarget)
  
  train( epoch+1,  previousCost )   --7.7196009864807 
end



